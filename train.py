import argparse
import os
import sys

import numpy as np

from config.train_config import TrainConfig
from src.tf1_utils.dataset.mnist import load_data
from src.tf1_utils.utils.batch_generator import BatchGenerator
from src.tf1_utils.utils.logger import create_logger


def main():
    logger = create_logger('gradcam', 'log', True)

    parser = argparse.ArgumentParser()
    parser.add_argument('data_path', help='Path to MNIST data directory')
    parser.add_argument('--output', default='output', help='Folder to save checkpoint and event')
    arguments = parser.parse_args()

    train_images, train_labels, _, _ = load_data(arguments.data_path)
    train_images = np.expand_dims(train_images, axis=-1)

    # Shuffle data
    permutation = np.random.permutation(len(train_labels))
    train_images = train_images[permutation]
    train_labels = train_labels[permutation]

    # Split train
    val_index = int(0.9 * len(train_images))
    val_images = train_images[val_index:]
    train_images = train_images[:val_index]
    val_labels = train_labels[val_index:]
    train_labels = train_labels[:val_index]

    batch_generator_train = BatchGenerator(train_images, train_labels, TrainConfig.BATCH_SIZE)
    batch_generator_val = BatchGenerator(val_images, val_labels, TrainConfig.BATCH_SIZE)

    logger.info('-------- Starting train --------')
    logger.info('From command : ' + ' '.join(sys.argv))
    logger.info(f'Data shape : {train_images.shape} train, {len(val_labels)} validation data')

    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
    from src.train import train
    train(batch_generator_train, batch_generator_val, arguments, logger=logger)



if __name__ == '__main__':
    main()
