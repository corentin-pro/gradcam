import src.tf1_utils.utils.import_tf1 as tf

from src.tf1_utils.layers import Layer
from src.tf1_utils.network import Network


class MNISTNetwork(Network):

    def __init__(self, input_op, is_training=None, batch_norm_training=None):
        super().__init__(input_op)

        if is_training is not None:
            Layer.IS_TRAINING = is_training
        if batch_norm_training is not None:
            Layer.BATCH_NORM_TRAINING = batch_norm_training

        Layer.PADDING = 'VALID'

        network_output = self.conv2D(input_op, 16)
        network_output = self.conv2D(network_output, 16)
        network_output = self.conv2D(network_output, 16)
        network_output = self.conv2D(network_output, 32, strides=[1, 2, 2, 1])
        network_output = self.conv2D(network_output, 16, strides=[1, 2, 2, 1])
        network_output = self.conv2D(network_output, 10, kernel_size=4, batch_norm=False, activation=None)
        network_output = tf.reshape(network_output, (-1, 10))

        # network_output = self.conv2D(input_op, 32, kernel_size=5, strides=[1, 2, 2, 1])
        # network_output = self.conv2D(network_output, 64)
        # network_output = self.conv2D(network_output, 32)
        # network_output = self.conv2D(network_output, 16)
        # flatten_shape = network_output.shape.as_list()[1:]
        # flatten_shape = flatten_shape[0] * flatten_shape[1] * flatten_shape[2]
        # network_output = tf.reshape(network_output, (-1, flatten_shape))
        # network_output = self.fully_connected(network_output, 400)
        # network_output = self.fully_connected(network_output, 100)
        # network_output = self.fully_connected(network_output, 200)
        # network_output = self.fully_connected(network_output, 10, batch_norm=False, activation=None)

        self.output_op = network_output
