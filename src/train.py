import glob
import os
import resource
import shutil
import subprocess
import time

from config.train_config import TrainConfig
from src.tf1_utils.utils.batch_generator import BatchGenerator
from src.tf1_utils.utils.logger import DummyLogger


def train(batch_generator_train: BatchGenerator, batch_generator_val: BatchGenerator,
          args, logger=DummyLogger()):
    # import tensorflow as tf
    import src.tf1_utils.utils.import_tf1 as tf
    from src.tf1_utils.layers import Layer
    from src.tf1_utils.train_utils import gradcam
    from src.network import MNISTNetwork
    # manage memory(True->if we need then use, False->use all.This is same as default)
    gpu_options = tf.GPUOptions(allow_growth=True)
    session_config = tf.ConfigProto(gpu_options=gpu_options)

    if batch_generator_train.batch_data is None:
        batch_generator_train.next_batch()
    data_shape = batch_generator_train.batch_data.shape[1:]
    with tf.Session(config=session_config).as_default() as session:
        # Setting the placeholders
        data_placeholder = tf.placeholder(
            tf.uint8,
            shape=[None, *data_shape],
            name='data_placeholder')
        label_placeholder = tf.placeholder(
            tf.uint8,
            shape=[None],
            name='label_placeholder')
        batch_norm_placeholder = tf.placeholder(tf.bool, shape=[], name='batch_norm_training')

        train_summaries = []
        image_summaries = []

        image_summaries.append(tf.summary.image('input', data_placeholder, max_outputs=TrainConfig.IMAGE_MAX_OUTPUT))
        with tf.variable_scope('PreProcess'):
            data_op = tf.cast(data_placeholder, tf.float32, name='data_op')
            label_op = tf.cast(label_placeholder, tf.int32, name='label_op')
            label_hot_op = tf.one_hot(label_placeholder, 10)

        with tf.variable_scope('Network'):
            Layer.VERBOSE = 1
            Layer.LOGGER = logger
            Layer.METRICS = TrainConfig.NETWORK_SUMMARIES
            Layer.ACTIVATION = tf.nn.leaky_relu
            # Layer.BATCH_NORM_DECAY = 0.9
            def regularizer(input_op: tf.Tensor):
                name_info = input_op.name.split('/')
                return tf.nn.l2_loss(
                    input_op,
                    name='/'.join([
                        'Train', 'Regularization', name_info[-2],
                        '_'.join([*name_info[-1].split(':'), 'loss'])]))
            Layer.REGULARIZER = regularizer

            network = MNISTNetwork(data_op, True, batch_norm_placeholder)

        # -------- Train and Evaluate the Mode --------
        with tf.variable_scope('Train'):
            with tf.variable_scope('Loss'):
                # Cross entropy loss
                ce_loss_op = tf.reduce_mean(
                    tf.nn.softmax_cross_entropy_with_logits_v2(label_hot_op, network.output_op))

                # Regularization losses
                reg_loss_op = tf.reduce_sum(
                    [TrainConfig.REGULARIZATION_COEFF * reg_op
                     for reg_op in tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)])
                loss_op = ce_loss_op + reg_loss_op
                mean_ce_loss_op, mean_ce_loss_update_op = tf.metrics.mean(ce_loss_op)
                mean_reg_loss_op, mean_reg_loss_update_op = tf.metrics.mean(reg_loss_op)
                mean_loss_op, mean_loss_update_op = tf.metrics.mean(loss_op)
            with tf.variable_scope('Optimizer'):
                with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
                    train_op = tf.train.AdamOptimizer(TrainConfig.LEARNING_RATE).minimize(loss_op)
            with tf.variable_scope('Accuracy'):
                accuracy_op = tf.reduce_mean(
                    tf.cast(
                        tf.equal(tf.argmax(network.output_op, axis=-1, output_type=tf.int32), label_op),
                        dtype=tf.float32
                    ))
                mean_acc_op, mean_acc_update_op = tf.metrics.mean(accuracy_op)
            train_summaries.append(tf.summary.scalar('ce_loss', mean_ce_loss_op))
            train_summaries.append(tf.summary.scalar('reg_loss', mean_reg_loss_op))
            train_summaries.append(tf.summary.scalar('loss', mean_loss_op))
            train_summaries.append(tf.summary.scalar('accuracy', mean_acc_op))

            gradcam_op = gradcam(network.layers[2].output_op, network.output_op, label_hot_op)
            image_summaries.append(tf.summary.image(
                'gradcam', tf.expand_dims(gradcam_op, axis=-1), max_outputs=TrainConfig.IMAGE_MAX_OUTPUT))

        local_train_init_op = tf.initializers.variables(tf.local_variables('Train'))
        mean_update_op = tf.group([
            mean_ce_loss_update_op, mean_reg_loss_update_op, mean_loss_update_op, mean_acc_update_op])
        session.run(tf.global_variables_initializer())
        session.run(tf.local_variables_initializer())

        # -------- Training loop --------
        checkpoints = []
        min_checkpoint_accuracy = 0

        if args.output:
            if os.path.exists(args.output):
                shutil.rmtree(args.output)
            os.makedirs(args.output)

            if TrainConfig.NETWORK_SUMMARIES:
                for layer in network.layers:
                    train_summaries += layer.summaries.scalars
                    train_summaries += layer.summaries.histograms
                    train_summaries += layer.summaries.images
            summary_op = tf.summary.merge(train_summaries)
            image_op = tf.summary.merge(image_summaries)
            train_writer = tf.summary.FileWriter(
                os.path.join(args.output, "train"), session.graph, flush_secs=10)
            val_writer = tf.summary.FileWriter(
                os.path.join(args.output, "val"), session.graph, flush_secs=10)
            saver = tf.train.Saver(tf.global_variables(scope="Network"), max_to_keep=5)

            def get_var_info(var: tf.Variable):
                var_size = 1
                for size in var.shape:
                    var_size *= size
                var_size *= 1 if var.dtype == tf.uint8 else 4
                var_unit = 'B'
                if var_size > 1024:
                    var_size = var_size // 1024
                    var_unit = 'kiB'
                if var_size > 1024:
                    var_size = var_size // 1024
                    var_unit = 'MiB'
                return '{:42};{:16};{};{}{}'.format(var.name, str(var.shape), var.dtype, var_size, var_unit)
            with open(os.path.join(args.output, 'saved_vars.csv'), 'w') as save_file:
                save_file.write(
                    '\n'.join([get_var_info(var) for var in tf.global_variables(scope="Network")]))

            # Save source files
            for entry in glob.glob(os.path.join('config', '**', '*.py'), recursive=True) + glob.glob(
                    os.path.join('src', '**', '*.py'), recursive=True):
                dirname = os.path.join(args.output, 'code', os.path.dirname(entry))
                if not os.path.exists(dirname):
                    os.makedirs(dirname)
                shutil.copy2(entry, os.path.join(args.output, 'code', entry))
        else:
            train_writer = None

        logger.info('Training... ({} samples, {} steps per epoch)'.format(
            len(batch_generator_train.index_list), batch_generator_train.step_per_epoch))
        summary_period = int(batch_generator_train.step_per_epoch / TrainConfig.SUMMARIES_PER_EPOCH)
        if summary_period == 0:
            summary_period = 1
        image_period = int(batch_generator_train.step_per_epoch / TrainConfig.IMAGE_PER_EPOCH)
        if image_period == 0:
            image_period = 1
        train_start_time = time.time()
        benchmark_time = time.time()
        benchmark_step = 0

        try:
            # Initialize batch generators
            if batch_generator_train.batch_data is None:
                batch_generator_train.next_batch()
            if batch_generator_val.batch_data is None:
                batch_generator_val.next_batch()

            def save_summary(min_accuracy):
                nonlocal batch_generator_train
                nonlocal batch_generator_val
                nonlocal data_placeholder
                nonlocal label_placeholder
                nonlocal batch_norm_placeholder
                if train_writer:
                    train_loss, train_accuracy, summary = session.run(
                        [mean_loss_op, mean_acc_op, summary_op],
                        feed_dict={
                            data_placeholder: batch_generator_train.batch_data,
                            label_placeholder: batch_generator_train.batch_label,
                            batch_norm_placeholder: True})
                    train_writer.add_summary(summary, global_step=batch_generator_train.global_step)
                    session.run(local_train_init_op)

                    # Validation metrics
                    val_epoch = batch_generator_val.epoch
                    while val_epoch == batch_generator_val.epoch:
                        session.run(
                            mean_update_op,
                            feed_dict={
                                data_placeholder: batch_generator_val.batch_data,
                                label_placeholder: batch_generator_val.batch_label,
                                batch_norm_placeholder: False})
                        batch_generator_val.next_batch()
                    val_loss, val_accuracy, summary = session.run(
                        [mean_loss_op, mean_acc_op, summary_op],
                        feed_dict={
                            data_placeholder: batch_generator_val.batch_data,
                            label_placeholder: batch_generator_val.batch_label,
                            batch_norm_placeholder: False})
                    val_writer.add_summary(summary, global_step=batch_generator_train.global_step)
                    session.run(local_train_init_op)
                else:
                    val_loss = None
                    val_accuracy = None
                    train_loss, train_accuracy = session.run(
                        [mean_loss_op, mean_acc_op],
                        feed_dict={
                            data_placeholder: batch_generator_train.batch_data,
                            label_placeholder: batch_generator_train.batch_label,
                            batch_norm_placeholder: True})
                    session.run(local_train_init_op)

                min_accuracy = checkpoints[-1]['acc'] if checkpoints else -1
                final_accuracy = val_accuracy if val_accuracy else train_accuracy
                if final_accuracy > min_accuracy:
                    checkpoint_name = "checkpoint_acc_%0.03f" % train_accuracy
                    if len(checkpoints) > TrainConfig.MAX_CHECKPOINT:
                        removing_checkpoint = checkpoints.pop()
                        checkpoint_folder_content = os.listdir(args.output)
                        for entry in checkpoint_folder_content:
                            if entry.startswith(removing_checkpoint["name"]):
                                os.remove(os.path.join(args.output, entry))
                    checkpoints.append({
                        "acc": train_accuracy,
                        "name": checkpoint_name
                    })
                    saver.save(
                        session, os.path.join(args.output, checkpoint_name),
                        global_step=batch_generator_train.global_step)
                    checkpoints.sort(key=lambda x: x["acc"], reverse=True)
                    min_accuracy = checkpoints[-1]["acc"]

                speed = benchmark_step / (time.time() - benchmark_time)
                if val_loss is not None:
                    logger.info(
                        f'Global step {batch_generator_train.global_step:d}, '
                        f'loss: {train_loss:.02E} {val_loss:.02E}, '
                        f'acc {train_accuracy:.03f} {val_accuracy:.03f}, '
                        f'{speed:0.02f} steps/s, {speed * batch_generator_train.batch_size:0.02f} input/sec')
                else:
                    logger.info(
                        f'Global step {batch_generator_train.global_step:d}, '
                        f'loss: {train_loss:.02E}, acc {train_accuracy:.03f}, '
                        f'{speed:0.02f} steps/s, {speed * batch_generator_train.batch_size:0.02f} input/sec')
                return min_accuracy

            if saver:
                saver.save(session, os.path.join(args.output, "checkpoint_0"))
            while batch_generator_train.epoch <= TrainConfig.TRAIN_EPOCH:
                epoch = batch_generator_train.epoch
                logger.info(f'Epoch {epoch:d}')
                while epoch == batch_generator_train.epoch:
                    # if benchmark_step > 1:
                    #     speed = benchmark_step / (time.time() - benchmark_time)
                    #     print('Step {:d}, {:0.02f} steps/s, {:0.02f} input/sec     '.format(
                    #         batch_generator_train.step, speed, speed * TrainConfig.BATCH_SIZE), end='\r')
                    session.run(
                        [train_op, mean_update_op],
                        feed_dict={
                            data_placeholder: batch_generator_train.batch_data,
                            label_placeholder: batch_generator_train.batch_label,
                            batch_norm_placeholder: True})
                    if epoch >= TrainConfig.SKIP_FIRST_EPOCH:
                        if (batch_generator_train.global_step % summary_period) == (summary_period - 1):
                            min_checkpoint_accuracy = save_summary(min_checkpoint_accuracy)
                            benchmark_time = time.time()
                            benchmark_step = 0
                        if (batch_generator_train.global_step % image_period) == (image_period - 1):
                            images = session.run(
                                image_op,
                                feed_dict={
                                    data_placeholder: batch_generator_train.batch_data,
                                    label_placeholder: batch_generator_train.batch_label,
                                    batch_norm_placeholder: True
                                })
                            train_writer.add_summary(images, global_step=batch_generator_train.global_step)
                    batch_generator_train.next_batch()
                    benchmark_step += 1
        except KeyboardInterrupt:
            pass
        for _ in range(30):
            session.run(
                [mean_update_op],
                feed_dict={
                    data_placeholder: batch_generator_train.batch_data,
                    label_placeholder: batch_generator_train.batch_label,
                    batch_norm_placeholder: True})
            batch_generator_train.next_batch()
        save_summary(min_checkpoint_accuracy)

        train_stop_time = time.time()

        train_memory_peak = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
        train_gpu_memory = subprocess.check_output(
            "nvidia-smi --query-gpu=memory.used --format=csv,noheader", shell=True).decode()

        if "CUDA_VISIBLE_DEVICES" in os.environ:
            train_gpu_memory = train_gpu_memory.split("\n")[int(os.environ["CUDA_VISIBLE_DEVICES"])]
        else:
            train_gpu_memory = " ".join(train_gpu_memory.split("\n"))

        result = "Training time : %gs\n\tRAM peak : %g MB\n\tVRAM usage : %s" % (
            train_stop_time - train_start_time,
            int(train_memory_peak / 1024),
            train_gpu_memory)
        logger.info(result)
